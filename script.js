//#region Init

if (gameRunning() == null) createGameStorate();

setVariables();

//#endregion

function setVariables() {
    mainTitle.innerText = localStorage.getItem('countryName');
    mandateProgressBar.children[0].style.width = (localStorage.getItem('currentMonth') % 52) / 0.52 + "%"
}