function createGameStorate() {
    gameRunning(true);
    localStorage.setItem('countryName', "United States of Cuteness");
    localStorage.setItem('currentMonth', 42);
}

function gameRunning(newState = null) {
    if (newState == null) return localStorage.getItem('gameRunning');
    localStorage.setItem('gameRunning', true);
}